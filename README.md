# XNAT Metadata Queries for Neurobridges Integration #

This repository includes a number of SQL files containing queries that can be executed to provide
metadata about projects on XNAT instances deployed in the field. These queries are intended to
provide information about the types of projects hosted on XNATs around the world�including
numbers of imaging sessions and subjects, keywords and descriptions of projects, and so on�
to integrate those instances into the federated search functions of the [Neurobridges](https://neurobridges.org)
project.

## Available queries ##

There are four separate queries available to run. All of them perform the same basic function,
differing only in the projects that are included in the metadata query:

* [xnat-deployment-data-all-projects.sql](xnat-deployment-data-all-projects.sql) captures data from _all_ projects, including private projects
* [xnat-deployment-data-public-and-protected-projects.sql](xnat-deployment-data-public-and-protected-projects.sql) only captures data from *public* and *protected* projects
* [xnat-deployment-data-public-projects.sql](xnat-deployment-data-public-projects.sql) only captures data from *public* projects
* [xnat-deployment-data-selected-projects.sql](xnat-deployment-data-selected-projects.sql) only captures data from projects *explicitly specified in the query*

The last query requires editing to add the ID of the projects you want to include.

> The folder [9.4](9.4) contains versions of each of these queries that have been modified to work with PostgreSQL
 9.4. You should _only_ use those queries if you are running PostgreSQL 9.4! If you have 9.5 or later, please use
 the queries in the top-level folder.

## What data is collected? ##

The queries included in this repository only collect aggregate data from each project, including:

* ID, secondary ID, name, description, and type
* Any keywords added to the project
* Number of subjects, shared subjects (i.e. subjects shared _into_ the project), and total subjects
* Number of experiments, shared experiments (i.e. experiments shared _into_ the project), and total experiments
* The primary investigator and any collaborators
* Publications associated with the project
* Custom fields associated with the project
* Study protocol

In addition to the project metadata, the queries also capture some site-wide information, including:

* Site ID
* Site URL
* Primary administrator's email address

Because the process of collecting this data is relatively manual, you can review all data collected
before submitting it.

## Running queries ##

Once you've selected which query you want to run, use the `psql` command-line client with the `--tuples-only`
(`-t`) and `--no-align` (`-A`) options. Depending on your database and hosting configuration, you may also need
to specify the `--host` (`-h`), `--port` (`-p`), `--username` (`-U`), and possibly other options to access the
database.

Redirect the output from this query into a file with a name that indicates where the data came from, e.g. your
institution/university name, lab, PI, etc., with the extension `json`.

A sample command line for running this query would be something like:

```
psql --username=xnat --tuples-only --no-align --file=xnat-deployment-data-all-projects.sql > miskatonic-med-school.json
```

## Submitting results ##

Once you've run the query and have reviewed your generated data, you can submit the JSON file at the following
URL:

[https://wustl.app.box.com/f/ee4e29d40f43438ab054e7ef944e4033](https://wustl.app.box.com/f/ee4e29d40f43438ab054e7ef944e4033)

Please upload a zip file that contains the output json and a separate README file with contact information (site name, name/email for followup questions).
We ask for this separate file in the event that the output does not capture that and/or the administrator might not be the person who will work with us.

A [JSON file containing sample output](sample-output.json) is included for reference. Note that the JSON generated
by the query you run _won't_ be pretty-printed like this sample: it was formatted for readability. For ease of review,
you can re-format the generated JSON in a few ways:

* With [jq ("a lightweight and flexible command-line JSON processor")](https://stedolan.github.io/jq/), you can format
  the output with a simple command: `jq . miskatonic-med-school.json`
* You can wrap the last selector in the query (`jsonb_build_object()`) in `jsonb_pretty()`: `jsonb_pretty(jsonb_build_object(...))`.
  Note that `jsonb_pretty()` is _not_ available in PostgreSQL 9.4.
* If you have Python version 2.6 or later installed, you can use `json.tool` to format your JSON:
  `cat miskatonic-med-school.json | python -m json.tool`

