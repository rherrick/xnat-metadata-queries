--
-- xsearch: xnat-deployment-data-selected-projects.sql
-- XNAT http://www.xnat.org
-- Copyright (c) 2022, Washington University School of Medicine
-- All Rights Reserved
--
-- Released under the Simplified BSD.
--

--
-- NOTE: This query is provided for compatibility with PostgreSQL 9.4. If you are running ANY version
-- of PostgreSQL later than 9.4, please use the corresponding query without the "-9.4" tag in the
-- file name.
--
-- This SQL file should be run using the psql command-line client with the --tuples-only (-t) and
-- --no-align (-A) options. Depending on your database and hosting configuration, you may also need
-- to specify the --host (-h), --port (-p), --username (-U), and other options to access the database.
--
-- You should redirect the output from this query into a file with a name that indicates where the data
-- came from, e.g. your institution/university name, lab, PI, etc., with the extension "json".
--
-- A sample command line for running this query would be something like:
--
--  psql --username=xnat --tuples-only --no-align \\
--       --file=xnat-deployment-data-selected-projects.sql > miskatonic-med-school.json
--
-- This query is different from other queries available for this project in that you need to specify
-- which projects you want to have included in the output. To do this, search for the following line:
--
--  p.id IN ('REPLACE', 'WITH', 'IDS', 'FOR', 'PROJECTS', 'TO', 'BE', 'INCLUDED')),
--
-- Remove everything within the parentheses and replace with the IDs of the projects you want to include.
-- For example, if you only want to include the projects Project1 and Project2, change that line to:
--
--  p.id IN ('Project1', 'Project2')),
--
-- Once you've added your project IDs, save the modified file and run the query from there.
--
-- You can change the scope of projects captured by using one of the other queries included in this
-- repository, including:
--
--  * xnat-deployment-data-all-projects.sql captures ALL projects, including private
--  * xnat-deployment-data-public-and-protected-projects.sql is limited to public and protected projects
--  * xnat-deployment-data-public-projects.sql is limited to public projects
--

set escape_string_warning = off;

WITH
    subject_counts AS (SELECT project, count(project) AS count FROM xnat_subjectdata GROUP BY project),
    shared_subject_counts AS (SELECT project, count(project) AS count FROM xnat_projectparticipant GROUP BY project),
    experiment_counts AS (SELECT project, count(project) AS count FROM xnat_experimentdata GROUP BY project),
    shared_experiment_counts AS (SELECT project, count(project) AS count FROM xnat_experimentdata_share GROUP BY project),
    study_protocol AS (SELECT xdat_meta_element_id FROM xdat_meta_element WHERE element_name = 'xnat:studyProtocol'),
    projects AS (
        SELECT
            p.id,
            p.secondary_id,
            p.name,
            p.description,
            p.type,
            coalesce(sc.count, 0)                            AS subjects,
            coalesce(ssc.count, 0)                           AS shared_subjects,
            coalesce(sc.count, 0) + coalesce(ssc.count, 0)   AS total_subjects,
            coalesce(ec.count, 0)                            AS experiments,
            coalesce(sec.count, 0)                           AS shared_experiments,
            coalesce(ec.count, 0) + coalesce(sec.count, 0)   AS total_experiments,
            CASE
                WHEN substring(p.keywords, '\\S(?:.*\\S)*') IS NULL
                    THEN NULL
                ELSE regexp_split_to_array(coalesce(p.keywords, ''), '\\s+')
                END                                          AS keywords,
            CASE
                WHEN substring(coalesce(pi.title, pi.firstname, pi.lastname, pi.institution, pi.department, pi.email, pi.phone), '\\S(?:.*\\S)*') IS NULL
                    THEN NULL
                ELSE json_build_object('title', substring(coalesce(pi.title, ''), '\\S(?:.*\\S)*'),
                                       'firstName', substring(coalesce(pi.firstname, ''), '\\S(?:.*\\S)*'),
                                       'lastName', substring(coalesce(pi.lastname, ''), '\\S(?:.*\\S)*'),
                                       'institution', substring(coalesce(pi.institution, ''), '\\S(?:.*\\S)*'),
                                       'department', substring(coalesce(pi.department, ''), '\\S(?:.*\\S)*'),
                                       'email', substring(coalesce(pi.email, ''), '\\S(?:.*\\S)*'),
                                       'phone', substring(coalesce(pi.phone, ''), '\\S(?:.*\\S)*'))
                END                                          AS pi,
            CASE
                WHEN substring(coalesce(i.title, i.firstname, i.lastname, i.institution, i.department, i.email, i.phone), '\\S(?:.*\\S)*') IS NULL
                    THEN NULL
                ELSE json_build_object('title', substring(coalesce(i.title, ''), '\\S(?:.*\\S)*'),
                                       'firstName', substring(coalesce(i.firstname, ''), '\\S(?:.*\\S)*'),
                                       'lastName', substring(coalesce(i.lastname, ''), '\\S(?:.*\\S)*'),
                                       'institution', substring(coalesce(i.institution, ''), '\\S(?:.*\\S)*'),
                                       'department', substring(coalesce(i.department, ''), '\\S(?:.*\\S)*'),
                                       'email', substring(coalesce(i.email, ''), '\\S(?:.*\\S)*'),
                                       'phone', substring(coalesce(i.phone, ''), '\\S(?:.*\\S)*'))
                END                                          AS investigator,
            CASE
                WHEN substring(coalesce(pr.title, pr.citation, pr.abstract, pr.commentary, pr.doi, pr.uri, pr.pubmed, pr.medline, pr.other, pr.type), '\\S(?:.*\\S)*') IS NULL
                    THEN NULL
                ELSE json_build_object('title', substring(coalesce(pr.title, ''), '\\S(?:.*\\S)*'),
                                       'citation', substring(coalesce(pr.citation, ''), '\\S(?:.*\\S)*'),
                                       'abstract', substring(coalesce(pr.abstract, ''), '\\S(?:.*\\S)*'),
                                       'commentary', substring(coalesce(pr.commentary, ''), '\\S(?:.*\\S)*'),
                                       'locator', substring(coalesce(pr.doi, pr.uri, ''), '\\S(?:.*\\S)*'),
                                       'pubmed', substring(coalesce(pr.pubmed, ''), '\\S(?:.*\\S)*'),
                                       'medline', substring(coalesce(pr.medline, ''), '\\S(?:.*\\S)*'),
                                       'other', substring(coalesce(pr.other, ''), '\\S(?:.*\\S)*'),
                                       'type', substring(coalesce(pr.type, ''), '\\S(?:.*\\S)*'))
                END                                          AS publication,
            CASE
                WHEN substring(coalesce(ap.name, ap.description, ap.data_type), '\\S(?:.*\\S)*') IS NULL
                    THEN NULL
                ELSE json_build_object('name', substring(coalesce(ap.name, ''), '\\S(?:.*\\S)*'),
                                       'description', substring(coalesce(ap.description, ''), '\\S(?:.*\\S)*'),
                                       'dataType', substring(coalesce(ap.data_type, ''), '\\S(?:.*\\S)*'))
                END                                          AS studyProtocol,
            CASE
                WHEN substring(pdf.name, '\\S(?:.*\\S)*') IS NULL
                    THEN NULL
                ELSE json_build_object(substring(pdf.name, '\\S(?:.*\\S)*'), substring(coalesce(pdf.field, ''), '\\S(?:.*\\S)*'))
                END                                          AS field,
            substring(coalesce(a.alias, ''), '\\S(?:.*\\S)*') AS alias
        FROM
            xnat_projectdata p
                LEFT JOIN subject_counts sc ON p.id = sc.project
                LEFT JOIN shared_subject_counts ssc ON p.id = ssc.project
                LEFT JOIN experiment_counts ec ON p.id = ec.project
                LEFT JOIN shared_experiment_counts sec ON p.id = sec.project
                LEFT JOIN xnat_investigatordata pi ON p.pi_xnat_investigatordata_id = pi.xnat_investigatordata_id
                LEFT JOIN xnat_projectdata_investigator pdi ON p.id = pdi.xnat_projectdata_id
                LEFT JOIN xnat_investigatordata i ON pdi.xnat_investigatordata_xnat_investigatordata_id = i.xnat_investigatordata_id
                LEFT JOIN xnat_projectdata_alias a ON p.id = a.aliases_alias_xnat_projectdata_id
                LEFT JOIN xnat_publicationresource pr ON p.id = pr.publications_publication_xnat_p_id
                LEFT JOIN xnat_abstractprotocol ap ON p.id = ap.xnat_projectdata_id AND ap.extension = (SELECT xdat_meta_element_id FROM study_protocol)
                LEFT JOIN xnat_projectdata_field pdf ON p.id = pdf.fields_field_xnat_projectdata_id
        WHERE
            p.id IN ('REPLACE', 'WITH', 'IDS', 'FOR', 'PROJECTS', 'TO', 'BE', 'INCLUDED')),
    aggregated AS (SELECT
                       id,
                       secondary_id                                                     AS secondaryId,
                       name,
                       description,
                       keywords,
                       type,
                       subjects,
                       shared_subjects                                                  AS sharedSubjects,
                       total_subjects                                                   AS totalSubjects,
                       experiments,
                       shared_experiments                                               AS sharedExperiments,
                       total_experiments                                                AS totalExperiments,
                       json_agg(pi)                                                     AS primaryInvestigator,
                       json_agg(investigator) FILTER (WHERE investigator IS NOT NULL)   AS investigators,
                       json_agg(publication) FILTER (WHERE publication IS NOT NULL)     AS publications,
                       json_agg(field) FILTER (WHERE field IS NOT NULL)                 AS fields,
                       json_agg(studyProtocol) FILTER (WHERE studyProtocol IS NOT NULL) AS studyProtocols
                   FROM
                       projects
                   GROUP BY id, secondary_id, name, description, keywords, type, subjects, shared_subjects, total_subjects, experiments, shared_experiments, total_experiments
                   ORDER BY id, secondary_id, name),
    all_json AS (
        SELECT
            to_json(row) AS project
        FROM
                (SELECT * FROM aggregated ORDER BY id) row),
    project_array AS (
        SELECT
            json_agg(project) AS projects
        FROM
            all_json),
    siteInfo AS (SELECT
                     p.name,
                     p.value
                 FROM
                     xhbm_preference p
                         LEFT JOIN xhbm_tool t ON p.tool = t.id
                 WHERE
                     t.tool_id = 'siteConfig' AND
                     p.name IN ('adminEmail', 'siteId', 'siteUrl'))
SELECT
    json_build_object('siteId', (SELECT value FROM siteInfo WHERE name = 'siteId'),
                      'siteUrl', (SELECT value FROM siteInfo WHERE name = 'siteUrl'),
                      'adminEmail', (SELECT value FROM siteInfo WHERE name = 'adminEmail'),
                      'projects', (SELECT projects FROM project_array));

set escape_string_warning = on;

